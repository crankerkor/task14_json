package com.oleksandr.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oleksandr.view.Paper;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class JacksonJSONHandler {
    private ObjectMapper mapper;

    public JacksonJSONHandler() {
        mapper = new ObjectMapper();
    }

    public List<Paper> parseJSON(File json) throws IOException {
        Paper[] papers;

        papers = mapper.readValue(json, Paper[].class);

        return Arrays.asList(papers);
    }

}
