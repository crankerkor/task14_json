package com.oleksandr.handlers;

import com.oleksandr.view.Chars;
import com.oleksandr.view.Paper;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SimpleJSONHandler {

    public List<Paper> parseJSON(File json) throws
            IOException, ParseException {
        List<Paper> papers = new ArrayList<>();

        JSONParser parser = new JSONParser();

        Object obj = parser.parse(new FileReader(json));

        JSONArray jsonArray = (JSONArray) obj;

        Iterator<JSONObject> iterator = jsonArray.iterator();

        while (iterator.hasNext()) {
            JSONObject jsonObject = iterator.next();

            Paper paper = new Paper();

            String title = (String) jsonObject.get("title");
            paper.setTitle(title);

            String type = (String) jsonObject.get("type");
            paper.setType(type);

            boolean isMonthly = (boolean) jsonObject.get("isMonthly");
            paper.setIsMonthly(isMonthly);

            Chars chars = new Chars();

            JSONObject charsObj = (JSONObject) jsonObject.get("chars");


            boolean isColored = (boolean) charsObj.get("isColored");
            chars.setIsColored(isColored);

            long pages = (long) charsObj.get("pages");
            chars.setPages((int) pages);

          if (charsObj.get("index") != null) {
            long index = (long) charsObj.get("index");
            chars.setIndex((int) index);
                }

            boolean isGlossy = (boolean) charsObj.get("isGlossy");
            chars.setIsGlossy(isGlossy);

            paper.setChars(chars);
            papers.add(paper);
        }

        return papers;



    }
}
