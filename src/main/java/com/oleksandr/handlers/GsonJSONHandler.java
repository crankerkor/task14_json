package com.oleksandr.handlers;

import com.google.gson.Gson;
import com.oleksandr.view.Paper;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class GsonJSONHandler {

    public List<Paper> parseJSON(File json) throws IOException {
        Paper[] papers;

        Gson gsonParser = new Gson();

        papers = gsonParser.fromJson(new FileReader(json),
                Paper[].class);

        return Arrays.asList(papers);
    }
}
