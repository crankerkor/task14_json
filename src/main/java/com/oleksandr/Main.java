package com.oleksandr;

import com.oleksandr.handlers.GsonJSONHandler;
import com.oleksandr.handlers.JacksonJSONHandler;
import com.oleksandr.handlers.SimpleJSONHandler;
import com.oleksandr.validators.JSONValidator;
import com.oleksandr.view.Paper;

import java.io.File;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        try {
            SimpleJSONHandler simpleJSONHandler = new SimpleJSONHandler();
            JacksonJSONHandler jacksonJSONHandler = new JacksonJSONHandler();
            GsonJSONHandler gsonJSONHandler = new GsonJSONHandler();

            JSONValidator validator = new JSONValidator();

            File json = new File("C:\\Users\\sashk\\Desktop\\string\\" +
                    "task14_JSON\\src\\main\\resources\\JSON\\periodicals.JSON");

            File jsonSchema = new File(
                    "C:\\Users\\sashk\\Desktop\\string\\" +
                            "task14_JSON\\src\\main\\resources\\JSON\\periodicalsSchema.json"
            );

            if (validator.isJsonValid(jsonSchema, json)) {
                // List<Paper> papers = simpleJSONHandler.parseJSON(json);
                List<Paper> papers = gsonJSONHandler.parseJSON(json);
                printList(papers);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private static void  printList(List<Paper> list) {
        for (Paper paper : list) {
            System.out.println(paper);
        }
    }
}
